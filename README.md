# Tutorial: Extending the Confluence Insert Link Dialog

This project is a tutorial for developers learning to write a insert link plugin for Confluence.
You can find the entire tutorial at: [Extending the Confluence Insert Link Dialog][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run
    
 [1]: https://developer.atlassian.com/display/CONFDEV/Extending+the+Confluence+Insert+Link+Dialog
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project